import sqlite3

# THIS SCRIPT IS OUTDATED SINCE OTHER ATTRIBUTES WHERE ADDED 
# TO THE TABLES IN ACCESS BUT IT IS STILL USEFULL.

# Connect to the database (creates a new database if it doesn't exist)
conn = sqlite3.connect('fms_database/database.db')
cursor = conn.cursor()

def create_db():
    # Create the 'Parts' table
    cursor.execute('''CREATE TABLE IF NOT EXISTS Parts (
                        part_id INTEGER PRIMARY KEY NOT NULL,
                        part_name CHAR(50) NOT NULL
                    )''')


    # Create the 'Processes' table
    cursor.execute('''CREATE TABLE IF NOT EXISTS Processes (
                        process_id INTEGER PRIMARY KEY,
                        part_id INTEGER,
                        machine_id INTEGER NOT NULL,
                        process_name CHAR(50) NOT NULL,
                        phase_num INTEGER NOT NULL ,
                        start_time DATE NOT NULL,
                        duration REAL NOT NULL,
                        FOREIGN KEY (part_id) REFERENCES Parts(part_id),
                        FOREIGN KEY (machine_id) REFERENCES Machines(machine_id)
                    )''')

    # Create the 'Machines' table
    cursor.execute('''CREATE TABLE IF NOT EXISTS Machines (
                        machine_id INTEGER PRIMARY KEY NOT NULL,
                        model_name CHAR(50) NOT NULL
                    )''')

    # Create the 'Parameters' table
    cursor.execute(''' CREATE TABLE IF NOT EXISTS Parameters(
                        parameter_id INTEGER PRIMARY KEY NOT NULL,
                        process_id INTEGER NOT NULL,
                        name CHAR(50) NOT NULL,
                        value REAL NOT NULL,
                        FOREIGN KEY (process_id) REFERENCES Processes(process_id)
                    )''')

def populate_db():
    part_id = [i for i in range(1,10)]
    machine_id = [i for i in range(6)]
    machine_type = ["TC","TC","VMC","VMC","GC"] # TurningCenter, VerticalMachiningCenter, GrindingCenter
    part_name = ["part_%d"%i for i in range(10)]

    for i in range(1,len(part_id)): 
        cursor.execute("INSERT INTO Parts (part_id) VALUES (?)",(part_id[i],))   
        cursor.execute("INSERT INTO Parts (part_name) VALUES (?)",(part_name[i],))
        # conn.commit()
        


create_db()
populate_db() 


# Commit the changes and close the connection
conn.commit()
conn.close()


