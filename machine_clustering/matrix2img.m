function binary_image = matrix2img(input_matrix,row_names,col_names)
    % Convert input matrix to binary image
    binary_image = input_matrix >= 1;
%     binary_image = input_matrix; 

    % Create grid
    [rows, cols] = size(input_matrix);
%     x_axis = repmat(1:cols, rows+1, 1)
%     y_axis = repmat((1:rows).', 1, cols+1);
    x_axis = 1 : cols ;
    y_axis = 1 : rows ;

    % Plot binary image with grid and indices
    figure("Position",[300,300,1600,700])
    im = imagesc(x_axis, y_axis, binary_image);
%     colormap(turbo)
%     colorbar
    colormap(gray)
    im.AlphaData = .7;
    ax = gca ; 
    set(gca, 'XTick', 1:cols, 'YTick', 1:rows, 'XTickLabel',col_names, 'YTickLabel', row_names);
    xlim([0,cols])
    axis equal tight;
    ax.TickLength = [0,0];
%     grid on;
end
