function [new_matrix,new_wp_headers] = max_energy_col(base_col_idx,old_matrix,old_wp_headers)
    % This function finds the max energy column and places it next to the current running column
    % base_col_idx is the collumn with which we are comparing every other column
    [nrows,ncols] = size(old_matrix);
    energy = zeros(1,ncols) ; 
    for i = base_col_idx + 1 : ncols  
        for j = 1 : nrows 
            % Original method : Energy += 1 only if both cells are 1 
%             if (old_matrix(j,i)>=1 && old_matrix(j,base_col_idx)>=1)
%                 energy(i) = energy(i) + 1 ;
%             end 
            
            % Enhanced method : Energy += the pruduct of the 2 cells 
            energy(i) = energy(i) + old_matrix(j,i) * old_matrix(j,base_col_idx) ; 
        end
    end

    [~,max_e_col_idx] = max(energy); % max_e_col_idx is the argmax of the energy vector

    % swap columns
    new_matrix = old_matrix ; 
    new_wp_headers = old_wp_headers ; 
    % Do not perform col swap if the max e col is one of the already placed cols 
    if(max_e_col_idx <= base_col_idx)
        return
    end 
    new_matrix(:,base_col_idx+1) = old_matrix(:,max_e_col_idx); 
    new_wp_headers(base_col_idx+1) = old_wp_headers(max_e_col_idx);
    new_matrix(:,max_e_col_idx) = old_matrix(:,base_col_idx+1);    
    new_wp_headers(max_e_col_idx) = old_wp_headers(base_col_idx+1); 
end 