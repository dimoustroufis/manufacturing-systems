function [cell_idx,group_idx] = cell_division(matrix,cellnum,groupnum)
    % Matrix is assumed to have undergone re-arrangement through the bond energy method.
    % First the energy of consecutive rows and is calculated. Those are normalized to the range [0,1]. 
    % The energies are sorted and the indices of the minimum elements are found. 
    % If the user requests n cells, then n-1 indices are returned because they represent the cell-breakers.
    % Cells refer to the rows (machines), while groups refer to the columns (workitems).
    % The indices on cell_idx and group_idx show where the cells and groups close. A vector of [2,4] means that 
    % the cell would contain [M1-M2], [M3-M4], [M4-everything else]. 

    [e_rows,e_cols,~] = energy(matrix); 
    e_rows = e_rows ./ max(e_rows); % normalize to [0-1]
    e_cols = e_cols ./ max(e_cols); % normalize to [0-1]
%     cell_thres = cellnum/size(matrix,1) ; % cell number divided by the number of machines
%     group_thres = groupnum/size(matrix,2) ; % group number divided by the number of workitems
%     cell_thres = interp1([0,1],[min(e_rows),max(e_rows)],cell_thres); % map cell threshold in the range of row energies
%     group_thres = interp1([0,1],[min(e_cols),max(e_cols)],group_thres); % map group threshold in the range of col energies


%     cell_idx = zeros(1,cellnum); % initial length equal to the rows -- will cut later
%     group_idx = zeros(1,size(matrix,2)); % initial length equal to the collumns -- will cut later
    
    [~,sorted_idx] = sort(e_rows); % short the rows in increasing order and get the idx of the elements in the original list
    cell_idx = sorted_idx(1:cellnum-1); % choose only as many elements as the number of cells the user defines
    cell_idx = sort(cell_idx); % sort for user convenience

    [~,sorted_idx] = sort(e_cols); % short the cols in increasing order and get the idx of the elements in the original list
    group_idx = sorted_idx(1:groupnum-1); % choose only as many elements as the number of groups the user defines
    group_idx = sort(group_idx); % sort for user convenience

end 