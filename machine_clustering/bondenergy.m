clear all
clc 
format compact 

 %% Processing for the assignment problem
filename = "workallocation.csv"; 
data = readtable(filename); 
% Break down data to a header vector an a numeric matrix
header = data.Properties.VariableNames;
matrix = table2array(data);
wp_headers = string(matrix(:,1)'); % get workpiece headers 
machine_headers = string(header(3:end).'); % get machine headers and convert it to a column vector
matrix = matrix(:,2:end); % pop out the first col containing item numbers. 

% Multiply production times with demand, then pop out the demand column
matrix = matrix .* matrix(:,1); 
matrix = matrix(:,2:end);
matrix = matrix .' ; % transpose to have machines as the rows.
[nrows,ncols] = size(matrix);


%% Run the clustering algorithm
nseeds = 10 ;
Gamma_max = 0 ;
opt_num_nz_cells = 0; 
opt_matrix = matrix ;
opt_machine_headers = machine_headers ; 
opt_wp_headers = wp_headers ; 
opt_cellnum = 0 ; 
opt_groupnum = 0 ; 

for colstart = 1 : ncols
    for rowstart = 1 : nrows 
%         rng(seed);
    
        % Initialize new matrix and headers
        new_matrix = matrix ; 
        new_machine_headers = machine_headers ; 
        new_wp_headers = wp_headers ; 
        
        % Step 1 : Select a column and place it first 
    %     col_init_idx = randi([1,ncols]); 
        col_init_idx = colstart ; 
        old_col = new_matrix(:,1); 
        old_wp_header = new_wp_headers(1);
        new_matrix(:,1) = new_matrix(:,col_init_idx); 
        new_wp_headers(1) = new_wp_headers(col_init_idx);
        new_matrix(:,col_init_idx) = old_col ;
        new_wp_headers(col_init_idx) = old_wp_header ; 
        
        % Step 2 : Re-organize the matrix column-wise
        %  no need to check for the max energy after the last column (it has nothing on its right)
        for i = 1 : ncols - 1 
            [new_matrix,new_wp_headers] = max_energy_col(i,new_matrix,new_wp_headers); 
        end 
        
        % Step 3 : Select a row and place it first 
    %     row_init_idx = randi([1,nrows]); 
        row_init_idx = rowstart ; 
        old_row = new_matrix(1,:);
        old_machine_header = new_machine_headers(1);
        new_matrix(1,:) = new_matrix(row_init_idx,:);
        new_machine_headers(1) = new_machine_headers(row_init_idx);
        new_matrix(row_init_idx,:) = old_row ; 
        new_machine_headers(row_init_idx) = old_machine_header ; 
        
        % Step 4 : Re-organize the matrix row-wise 
        % no need to check for the max energy after the last row (it has nothing below it)
        for i = 1 : nrows - 1
            [new_matrix,new_machine_headers] = max_energy_row(i,new_matrix,new_machine_headers); 
        end 
        
        %% Cell efficiency optimization based on cell division
    
        [cellnum,groupnum] = opt_cell_division(new_matrix);
        [Gamma,num_nz_cells,~,~] = clustering_efficiency(new_matrix,cellnum,groupnum);
    
        % Update max efficiency
        if Gamma > Gamma_max
            Gamma_max = Gamma ; 
            [cell_idx,group_idx] = cell_division(new_matrix,cellnum,groupnum); 
            opt_cellnum = cellnum ; 
            opt_groupnum = groupnum ; 
            opt_cell_idx = cell_idx ; 
            opt_group_idx = group_idx ; 
            opt_num_nz_cells = num_nz_cells ; 
            opt_matrix = new_matrix ; 
            opt_machine_headers = new_machine_headers ; 
            opt_wp_headers = new_wp_headers ; 
        end 
    end
end

%% Write results 
matrix2img(opt_matrix,opt_machine_headers,opt_wp_headers);
fprintf("The number of non-zero cells in the best scenario : %d \n",opt_num_nz_cells)
fprintf("The best achieved efficiency of the clustering : %f \n",Gamma_max)
fprintf("The process is divided into %d cells and %d groups \n",opt_cellnum,opt_groupnum)
fprintf("The following cells are formed : \n")
disp(opt_machine_headers(1:opt_cell_idx(1)).')
for i = 2 : opt_cellnum -2
    disp(opt_machine_headers(opt_cell_idx(i-1):opt_cell_idx(i)).')
end 
if(opt_cellnum > 2)
    disp(opt_machine_headers(opt_cell_idx(opt_cellnum-1):end).')
else 
    disp(opt_machine_headers(end).')
end 
fprintf("\n")

fprintf("The following groups are formed : \n")
disp(opt_wp_headers(1:opt_group_idx(1)))
for i = 2 : opt_groupnum -2
    disp(opt_wp_headers(opt_group_idx(i-1):opt_group_idx(i)))
end 
if(opt_groupnum > 2)
    disp(opt_wp_headers(opt_group_idx(groupnum-1):end))
else 
    disp(opt_wp_headers(end))
end 
