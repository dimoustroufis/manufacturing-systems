function efficiency = group_division_eff(new_matrix,cellnum,groupnum)  
    % This efficiency metric evaluates the clustering efficiency be means of the following quantity : 
    % average_on_all_workpieces(number of cells required for the workpiece)
    
    workpiece_num = size(new_matrix,2); 

    % Divide the acquired matrix form the bond energy method into cells and groups
    [cell_idx,~] = cell_division(new_matrix,cellnum,groupnum); 

    
    % Run each collumn (== each workpiece) and count the different number of cells encountered
    efficiency = 0 ;
    for i = 1 : workpiece_num
        for j = 1 : cellnum-1
            % Check the manufacturing hours of the workpiece in all machines of each cell 
            % If the maximum manufacturing hours are 0, then the workpiece does not belong to this cell
            % Due to the way the division into cells works, the first and last cells are checked separately
            if (j==1)
                incell = max(new_matrix(1:cell_idx(j),i));
                if(incell ~= 0) 
                    efficiency = efficiency + 1 ; 
                end
            
            elseif(j==cellnum-1)
                incell = max(new_matrix(cell_idx(j):end,i)); 
                if(incell~=0)
                    efficiency = efficiency + 1 ; 
                end 
            
            else 
                incell = max(new_matrix(cell_idx(j-1):cell_idx(j),i)); 
                if(incell ~=0)
                    efficiency = efficiency + 1 ; 
                end 
            end

        end
    end 

    efficiency = efficiency / workpiece_num ; 

end 