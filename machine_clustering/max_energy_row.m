function [new_matrix,new_machine_headers] = max_energy_row(base_row_idx,old_matrix,old_machine_headers)

    % This function finds the max energy column and places it next to the current running column
    % base_col_idx is the collumn with which we are comparing every other column
    [nrows,ncols] = size(old_matrix);
    energy = zeros(1,nrows) ; 
    for i = base_row_idx + 1 : nrows 
        for j = 1 : ncols 
            % Original method : Energy += 1 only if both cells are 1 
%             if (old_matrix(i,j) >= 1 && old_matrix(base_row_idx,j) >= 1)
%                 energy(i) = energy(i) + 1 ;
%             end 

            % Enhanced method : Energy += the pruduct of the 2 cells 
            energy(i) = energy(i) + old_matrix(i,j) * old_matrix(base_row_idx,j); 
        end 
    end

    [~,max_e_row_idx] = max(energy); % max_e_col_idx is the argmax of the energy vector

    % swap rows
    new_matrix = old_matrix ; 
    new_machine_headers = old_machine_headers ; 
    % Do not perform row swap if the max e row is one of the already placed rows 
    if(max_e_row_idx <= base_row_idx)
        return
    end 
    new_matrix(base_row_idx + 1,:) = old_matrix(max_e_row_idx,:); 
    new_machine_headers(base_row_idx+1) = old_machine_headers(max_e_row_idx);
    new_matrix(max_e_row_idx,:) = old_matrix(base_row_idx+1,:); 
    new_machine_headers(max_e_row_idx) = old_machine_headers(base_row_idx+1);
    end 