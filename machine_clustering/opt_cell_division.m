function [opt_cellnum,opt_groupnum] = opt_cell_division(new_matrix)
%% Input variables explanation:
% - new_matrix: The resultant matrix derived from bond energy method.

%% Output variables explanation:
% - opt_cellnum: the number of row cell divisions that gives the highest
% efficiency
% - opt_groupnum: the number of column cell divisions that gives the highest
%% Cell efficiency optimization based on cell division

[e_rows,e_cols] = size(new_matrix); 
max_gamma = 0;
opt_cellnum = 0;
opt_groupnum = 0;
for cellnum = (2:e_rows-1)
    for groupnum = (2:e_cols-1)
        Gamma = clustering_efficiency(new_matrix,cellnum,groupnum);
        if (Gamma > max_gamma)
            max_gamma = Gamma;
            opt_cellnum = cellnum;
            opt_groupnum = groupnum;
        end
    end
end