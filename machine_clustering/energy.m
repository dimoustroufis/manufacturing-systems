function [e_row,e_col,e_sum] = energy(matrix)
    % Returns the energy between consecutive rows and columns. 
    % he results have n-1 elemens, since the last row/col has no neighbors.

    [nrows,ncols] = size(matrix); 
    e_col = zeros(1,ncols-1);
    e_row = zeros(1,nrows-1);

    for i = 1 : nrows - 1  
        for j = 1 : ncols 
            % Original method 
%             if(matrix(i,j)>=1 && matrix(i+1,j)>=1)
%                 e_row(i) = e_row(i) + 1 ; 
%             end 
            e_row(i) = e_row(i) + matrix(i,j)*matrix(i+1,j) ; 
        end 
    end 

    for i = 1 : ncols -1 
        for j = 1 : nrows 
            % Original method
%             if(matrix(j,i)>=1 && matrix(j,i+1)>=1)
%                 e_col(i) = e_col(i) + 1 ; 
%             end 
            e_col(i) = e_col(i) + matrix(j,i) * matrix(j,i+1);
        end 
    end 
    
    e_sum = sum(e_row) + sum(e_col);

end 