# Manufacturing Systems

## Machine clustering 
The machine clustering folder contains code that clusters the machines of a factory into cells by taking into account the workitem production needs. Clustering is performed through a variation of the Bond Energy Method. The re-arranged machine-item matrix is automatically broken into machine cells and item groups.
